package Interface;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JPanel;


/**
 * Décrivez votre classe Main ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Main
{
    JFrame fenetre;
    JOptionPane jopt;
    JButton bRecrut,bAssocier,bListOp,bListC,bListP,bInfoP,bInfoC,bEtatC,bNouveauC;
    ActionListener al;
    ActionEvent e;
    JPanel jpHaut,jpBas;
    public Main(){
        fenetre=new JFrame("MainMenu");
        fenetre.setSize(800,500);
        fenetre.setLayout(new BoxLayout(fenetre.getContentPane(),BoxLayout.PAGE_AXIS));
        jpHaut=new JPanel();
        jpHaut.setSize(800,300);
        fenetre.add(jpHaut);
        jpBas=new JPanel();
        jpBas.setSize(800,200);
        fenetre.add(jpBas);
        jpBas.setLayout(new GridLayout(3,3,10,10));
        bRecrut=new JButton("Inscription Pilote");
        bAssocier=new JButton("Associer un chasseur à un pilote");
        bListOp=new JButton("Liste des chasseurs opérationnels");
        bListC=new JButton("Liste de tous les chasseurs");
        bListP=new JButton("Liste des pilotes");
        bInfoP=new JButton("Informations sur un Pilote");
        bInfoC=new JButton("Informations sur un Chasseur");
        bEtatC=new JButton("Modifier état d'un Chasseur");
        bNouveauC=new JButton("Enregistrement d'un nouveau Chasseur");
        jpBas.add(bRecrut);
        jpBas.add(bAssocier);
        jpBas.add(bNouveauC);
        jpBas.add(bInfoP);
        jpBas.add(bInfoC);
        jpBas.add(bListC);
        jpBas.add(bListP);
        jpBas.add(bEtatC);
        jpBas.add(bListOp);
        bRecrut.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new InscriptionRebelle();
            }
        });
        bAssocier.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AffecterChasseur();
            }
        });
        bNouveauC.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new ConstructionVaisseau();
            }
        });
        bInfoP.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AfficherPilote();
            }
        });
        bInfoC.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AfficherUnVaisseau();
            }
        });
        bListC.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
               new AfficherChasseurs();
            }
        });
        bListP.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AfficherPilotes();
            }
        });
        bListOp.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AfficherVaisseauOperationnel();
            }
        });
        bEtatC.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new ModifierVaisseau();
            }
        });
        fenetre.setVisible(true);
    }
}
