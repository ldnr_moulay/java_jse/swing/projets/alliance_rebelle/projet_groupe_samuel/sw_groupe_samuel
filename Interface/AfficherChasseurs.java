package Interface;


import Metier.*;
import javax.swing.JFrame;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javafx.scene.control.Label;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.util.Map;
import javafx.scene.control.DialogPane;
import java.awt.GridLayout;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 * @author(Samuel-Chantal)
 * Crée une fenetre avec un bouton
 * Si on clique sur le bouton la liste des informations des chasseurs est affichée
 * 
 */

public class AfficherChasseurs extends JFrame

{
    JFrame fenetre;
    JOptionPane jopt;
    JButton bAfficher;
    boolean isListAffichee=false;
    
    public AfficherChasseurs(){
        start();
    }

    
    public void start() 
    {
        fenetre=new JFrame("Affichage liste Pilote");
        
        fenetre.setSize(500,150+Chasseur.getListeChasseur().size()*50);
        fenetre.setLayout(new GridLayout(Chasseur.getListeChasseur().size()+1,1));
        bAfficher=new JButton("Afficher Liste des Chasseurs");
        bAfficher.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                afficherListeChasseur();
            }
        });
        
        fenetre.add(bAfficher);
        fenetre.setVisible(true);
    }
    

    
    /**
     * Fonction qui affiche les chasseurs. Elle est appelée quand on appuie sur le bouton
     */
    public void afficherListeChasseur(){
        Map<Integer, Chasseur> chasseur = Chasseur.getListeChasseur();
                 if(!isListAffichee){  
            for (Map.Entry<Integer, Chasseur> entry: chasseur.entrySet()){
               int numero  = entry.getKey();
               TypeVaisseau type =entry.getValue().getType();
               String nomtype = type.getNom();
               String etat = entry.getValue().getEtat();
               fenetre.add(new JLabel("chasseur : " + numero+" " + nomtype+" " + etat));
               fenetre.setVisible(true);
                isListAffichee=true;
                }
               if(Chasseur.getListeChasseur().size()==0){
                jopt.showMessageDialog(null, "Aucun chasseur créé ","Information", JOptionPane.INFORMATION_MESSAGE);
                }
        }
        }
}


