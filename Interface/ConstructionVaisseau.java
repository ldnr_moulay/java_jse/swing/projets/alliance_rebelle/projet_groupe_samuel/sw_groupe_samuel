package Interface;


import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JRadioButton;
import Metier.*;


/**
 * Classe qui nous permet de créer un objet vaisseau
 *
 * @author (Thomas)
 */
public class ConstructionVaisseau extends JFrame
{
    private Panel fenetre = new Panel();
    private JPanel conteneur = new JPanel();
    
    private JLabel titre = new JLabel("Nouveau Vaisseau");
    
    private JButton bouton = new JButton("Valider");
    
    private JRadioButton xwing = new JRadioButton("X-Wing");
    private JRadioButton ywing = new JRadioButton("Y-Wing");
    private ButtonGroup bg = new ButtonGroup();
    
    private JLabel etatTxt = new JLabel("L'état du vaisseau ");
    private String[] tab ={"Opérationnel", "En maintenance", "Détruit"};
    private JComboBox etat = new JComboBox(tab);
    
    private Boolean typeVaisseau;
    private String etatVaisseau;
   private int numero;
    
    public ConstructionVaisseau(){
        
        
        // propriété de la fenetre
        
        this.setTitle("Ajout d'un vaisseau au hangar");
        this.setSize(400,300);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);
        
        conteneur.setBackground(Color.white);
        conteneur.setLayout(new BorderLayout());
        
        fenetre.setBackground(Color.blue);
        fenetre.setLayout(new BorderLayout());
        
        // on définit les boutons radio
        JPanel top = new JPanel();
        xwing.setSelected(true);
        xwing.addActionListener(new StateListener());
        ywing.addActionListener(new StateListener());
        
        // on ajoute les boutons au groupe
        bg.add(xwing);
        bg.add(ywing);
        // on ajoute les bouton à la fenetre top
        top.add(xwing);
        top.add(ywing);
        /*
        // on définit la liste pour afficher l'état du vaisseau
        etat.addItemListener(new ItemState());*/
        //etat.addActionListener(new ItemAction());
        JPanel middle = new JPanel();
        etat.setPreferredSize(new Dimension(200,40));
        middle.add(etatTxt);
        middle.add(etat);
        // on assemble les fenetres entres elles
        
        fenetre.add(top,BorderLayout.NORTH);
        fenetre.add(middle,BorderLayout.CENTER);
        conteneur.add(fenetre,BorderLayout.CENTER);
        
        
        JPanel south = new JPanel();
        south.add(bouton);
        conteneur.add(south, BorderLayout.SOUTH);
        
        
        
        
        // On s'occupe du titre de la fenetre
        titre.setForeground(Color.blue);
        titre.setHorizontalAlignment(JLabel.CENTER);
        
        // ajout du titre au conteneur
        conteneur.add(titre, BorderLayout.NORTH);
        
        
        
        this.setContentPane(conteneur);
        
        
        // on actionne le bouton
        
        bouton.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e){
            
            valider();
            
            }
        });
        
        
        
        // on affiche la fenetre
        this.setVisible(true);

    }
    
    public void valider(){
       if(xwing.isSelected()){
            Xwing vaisseauSpatial = new Xwing();
            Chasseur chasseur = new Chasseur(vaisseauSpatial,etat.getSelectedItem().toString());
        }else if(ywing.isSelected()){
            Ywing vaisseauSpatial = new Ywing(); 
            Chasseur chasseur = new Chasseur(vaisseauSpatial,etat.getSelectedItem().toString());
        }
        this.dispose();
    }
    class StateListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            /*if(xwing.isSelected()){
                
                typeVaisseau = true;
                
            }else{
                 typeVaisseau = false;   
            }
            */
           /* System.out.println("source : " + xwing
            .getText() + " - état : " +xwing.isSelected());
            
            System.out.println("source : " + ywing
            .getText() + " - état : " +ywing.isSelected());
         */   
        }
        
        
    }
    
    /*class ItemState implements ItemListener{
        
            public void itemStateChanged(ItemEvent e){
                
                
                etatVaisseau =etat.getSelectedItem().toString();
                
                //System.out.println("etat déclenché : " + e.getItem());
                
            }
        
        
    }*/
    
    /*class ItemAction implements ActionListener{
        
            public void actionPerformed(ActionEvent e){
                
                
                etatVaisseau = "" + etat.getSelectedItem();
                
                //System.out.println("etat déclenché : " + e.getItem());
                
            }
        
        
    }*/
}
