package Interface;


/**
 * Affiche une fenetre avec un bouton, quand on clique dessus la liste des pilotes s'affiche
 *
 * @author Samuel
 */
import Metier.Pilote;
import javax.swing.JFrame;
import javafx.scene.control.DialogPane;
import javax.swing.JButton;
import Metier.Pilote;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.HashMap;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.FlowLayout;


public class AfficherPilotes
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    JFrame fenetre;
    DialogPane jopt;
    JButton bAfficher;
    boolean isListAffichee=false;
    
    public AfficherPilotes(){
        fenetre=new JFrame("Affichage liste Pilote");
        fenetre.setSize(500,150+Pilote.getListe().size()*50);
        fenetre.setLayout(new GridLayout(Pilote.getListe().size()+1,1));
        bAfficher=new JButton("Afficher Liste des Pilotes");
        bAfficher.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                afficherListe();
            }
        });
        
        fenetre.add(bAfficher);
        fenetre.setVisible(true);
    }
    
    private void afficherListe(){
        if(!isListAffichee){
            Map<Integer,Pilote> pilotes=Pilote.getListe();
            for(Map.Entry<Integer, Pilote> entry: pilotes.entrySet()){
                String prenom=entry.getValue().getPrenom();
                String nom=entry.getValue().getNom();
                String race = entry.getValue().getRace();
                int age = entry.getValue().getAge();
                int numP=entry.getKey();
                JPanel jp=new JPanel();
                jp.setLayout(new FlowLayout());
                jp.add(new JLabel("Prénom:"));
                jp.add(new JLabel(prenom));
                jp.add(new JLabel("Nom:"));
                jp.add(new JLabel(nom));
                jp.add(new JLabel("Race:"));
                jp.add(new JLabel(race));
                jp.add(new JLabel("Age:"));
                jp.add(new JLabel(age+""));
                jp.add(new JLabel("Numéro:"));
                jp.add(new JLabel(numP+""));
                fenetre.add(jp);
                fenetre.setVisible(true);
                }
            isListAffichee=true;
        }
    }
    }
    

