package Interface;


import java.awt.*;
import javax.swing.*;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.util.Map;
import java.util.HashMap;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Metier.*;
/**
* Créée une fenetre avec un champ pour mettre le numéro du chasseur et un bouton
* Quand on clique sur le bouton les caractéristiques du chasseurs sont affichées
*
* @author (Thomas)
* @version (03-05-2021)
*/
public class AfficherUnVaisseau extends JFrame
{
    private Panel fenetre = new Panel();
    private JPanel conteneur = new JPanel();
    private JLabel titre = new JLabel("Affichage d'un vaisseau");
    private JLabel listeTxt = new JLabel("Numéro d'identifiant ");
    
    private JTextField choixIdentifiant = new JTextField("0");
    
    private JButton bouton = new JButton("OK");
    
    private JLabel typeVaisseauTxt = new JLabel("Type de vaisseau :");
    private JLabel etatVaisseauTxt = new JLabel("Etat du vaisseau :");
    private JLabel typeVaisseau = new JLabel();
    private JLabel etatVaisseau = new JLabel();
    private JLabel etatPilote = new JLabel();
    
    private Xwing vaisseau = new Xwing();
    private Ywing vaisseau1 = new Ywing();
    private static Map < Integer, Chasseur> listeChasseur;
    private int saisie;
    Chasseur chasseur;
    /**
    * Constructeur d'objets de classe AfficherUnVaisseau
    */
    public AfficherUnVaisseau()
    {
        this.setTitle("Afficher un vaisseau");
        this.setSize(500,300);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        conteneur.setBackground(Color.white);
        conteneur.setLayout(new BorderLayout());
        
        // fenetre.setBackground(Color.blue);
        fenetre.setLayout(new BorderLayout());
        
        JPanel top = new JPanel();
        
        // appel de l'identifiant du chasseur dans un textField
        
        choixIdentifiant.setPreferredSize(new Dimension(150,30));
        
        
        
        top.add(listeTxt);
        top.add(choixIdentifiant);
        // affichage des diverses informations
        
        JPanel middle = new JPanel();
        middle.setLayout(new FlowLayout());
        middle.add(typeVaisseauTxt);
        middle.add(typeVaisseau);
        middle.add(new JLabel("      "));
        middle.add(etatVaisseauTxt);
        middle.add(etatVaisseau);
        middle.add(new JLabel("      "));
        middle.add(etatPilote);
        
        // propriété du boutton
        
        JPanel south = new JPanel();
        south.add(bouton);
        conteneur.add(south, BorderLayout.SOUTH);
        
        // assemblage des fenetres
        
        fenetre.add(top,BorderLayout.NORTH);
        fenetre.add(middle,BorderLayout.CENTER);
        conteneur.add(fenetre,BorderLayout.CENTER);
        
        this.setContentPane(conteneur);
        
        
        
        
        // Action sur le bouton Ok
        bouton.addActionListener(new ActionListener(){
            
            @Override
            public void actionPerformed(ActionEvent e){
                
                valider();
                
            }
        });
        this.setVisible(true);
        }
    public void valider(){
        JOptionPane jop1;
        try{
            saisie = Integer.parseInt(choixIdentifiant.getText());
        }catch(Exception e){
        
            jop1 = new JOptionPane();
            jop1.showMessageDialog(null,"Vous n'avez pas mis un entier","Message d'erreur",
            JOptionPane.ERROR_MESSAGE);
            this.dispose();
            new AfficherUnVaisseau();
        }
        
        try
        {
            typeVaisseau.setText("" + Chasseur.getChasseurNum(saisie).getType().getNom());
            etatVaisseau.setText("" + Chasseur.getChasseurNum(saisie).getEtat());
            if(Chasseur.getChasseurNum(saisie).getPilote()!=null){
                etatPilote.setText("Piloté par: "+Chasseur.getChasseurNum(saisie).getPilote().getPrenom()+" n° "+Chasseur.getChasseurNum(saisie).getPilote().getIdentifiant());
            }
        }
        catch (Exception e)
        {
            jop1 = new JOptionPane();
            jop1.showMessageDialog(null,"Erreur lors de la mise à jour de l'état","Message d'erreur",
            JOptionPane.ERROR_MESSAGE);
            this.dispose();
            new AfficherUnVaisseau();
        }
    
    }
    class ItemState implements ItemListener{
    
        public void itemStateChanged(ItemEvent e){
            
            /*
            typeVaisseau.setText("" + Chasseur.getListeChasseur().get(e.getItem()).getType());
            etatVaisseau.setText("" + Chasseur.getListeChasseur().get(e.getItem()).getEtat());
            */
            //System.out.println("etat déclenché : " + e.getItem());
            
        }
        
    
    }
}