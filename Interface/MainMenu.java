package Interface;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;


/**
 * Affiche un menu avec 9 boutons qui ouvriront les fenetre en fonction de la fonctionnalité recherchée
 *
 * @author Samuel
 */
public class MainMenu
{
    JFrame fenetre;
    JOptionPane jopt;
    JButton bRecrut,bAssocier,bListOp,bListC,bListP,bInfoP,bInfoC,bEtatC,bNouveauC;
    ActionListener al;
    ActionEvent e;
    JPanel jpHaut,jpBas;
    public MainMenu(){
        fenetre=new JFrame("MainMenu");

        fenetre.setSize(800,500);
        fenetre.setLayout(new BoxLayout(fenetre.getContentPane(),BoxLayout.PAGE_AXIS));
        jpHaut=new JPanel();
        jpHaut.setSize(800,300);
        fenetre.add(jpHaut);
        jpBas=new JPanel();
        jpBas.setSize(800,200);
        JLabel text=new JLabel("Star Wars");
        jpHaut.add(text);
        text.setFont(new Font("Serif",Font.BOLD,180));
        text.setForeground(new Color(240,247,30));
        text.setBackground(new Color(13,0,79));
        text.setOpaque(true);
        
        
        fenetre.add(jpBas);
        jpBas.setLayout(new GridLayout(3,3,10,10));
        bRecrut=new JButton("Inscription Pilote");
        bAssocier=new JButton("Associer un chasseur à un pilote");
        bListOp=new JButton("Liste des chasseurs opérationnels");
        bListC=new JButton("Liste de tous les chasseurs");
        bListP=new JButton("Liste des pilotes");
        bInfoP=new JButton("Informations sur un Pilote");
        bInfoC=new JButton("Informations sur un Chasseur");
        bEtatC=new JButton("Modifier état d'un Chasseur");
        bNouveauC=new JButton("Enregistrement d'un nouveau Chasseur");
        jpBas.add(bRecrut);
        jpBas.add(bAssocier);
        jpBas.add(bNouveauC);
        jpBas.add(bInfoP);
        jpBas.add(bInfoC);
        jpBas.add(bListC);
        jpBas.add(bListP);
        jpBas.add(bEtatC);
        jpBas.add(bListOp);
        jpHaut.setBackground(new Color(13,0,79));
        jpBas.setBackground(new Color(13,0,79));
        jpHaut.setOpaque(true);
        jpBas.setOpaque(true);
        bRecrut.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new InscriptionRebelle();
            }
        });
        bAssocier.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AffecterChasseur();
            }
        });
        bNouveauC.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new ConstructionVaisseau();
            }
        });
        bInfoP.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AfficherPilote();
            }
        });
        bInfoC.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AfficherUnVaisseau();
            }
        });
        bListC.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
               new AfficherChasseurs();
            }
        });
        bListP.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AfficherPilotes();
            }
        });
        bListOp.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new AfficherVaisseauOperationnel();
            }
        });
        bEtatC.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new ModifierVaisseau();
            }
        });
        fenetre.setVisible(true);
    }
}
