package Metier;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;


/**
 * Décrivez Classe pour créer des objets Pilote
 * @author (Chantal)
 * @version (1.0----03-05-2021)
 */
public class Pilote
{
    private int identifiant ;
    private String prenom ;
    private String nom ;
    private String race;
    private int age;
    private static Set <String> races = new HashSet<String>();
    private static Map < Integer, Pilote> pilote=new HashMap<>();
    private Chasseur chasseur;
    static boolean isListCree=false;
    
    /**
     * Constructeur de la classe pilote
     */
    public Pilote(String prenom,String nom,String race, int age) throws Exception
    {
            try{
            this.prenom = prenom;
            setNom(nom) ;
            setAge(age);
            setRace(race);
            identifiant=pilote.size()+1;
            pilote.put(this.identifiant,this);
        }
        catch(Exception ageE){
            throw ageE;
        }
    }
    
    
     /**
     * Getter et setters de la classe pilote
     */
    public String getRace(){
        return this.race;
    }
    
    public void setRace(String race){
        this.race=race;
    }
    
    public static Set<String> getRaces(){
        if(!isListCree){
            listeRaces();
        }
        return races;
    }
    
     public int getIdentifiant() {
        return identifiant;
    }
     public String getPrenom() {
        return prenom;
    }
     public String getNom() {
        return nom;
    }
      public int getAge() {
        return age;
    }
     public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }
     public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
     public void setNom(String nom) {
        this.nom = nom;
    }
      public void setAge(int age) throws Exception{
        if((age<10||age>800)){throw new Exception("le pilote doit avoir entre 10 et 800ans");}
        else{this.age = age;}
        
    }
    
    /**
     * Méthode qui remplit les différentes races existantes
     */
    public static void listeRaces()
    {
        races.add("Chalactéens");
        races.add("Chiss");
        races.add("Humains");
        races.add("Ithoriens");
        races.add("Mirialans");
        races.add("Kel'Dor");
        races.add("Kiffars");
        races.add("Miralukas");
        races.add("Nagais");
        races.add("Neimoidiens");
        races.add("Noghris");
        races.add("Ongrees");
        races.add("Pau'ans");
        races.add("Quermiens");
        races.add("Rakata");
        races.add("Rodiens");
        races.add("Thisspasiens");
        races.add("Togrutas");
        races.add("Wookies");
        races.add("Wronians");
        races.add("Zabraks");
        isListCree=true;
    }
    
    /**
     * Cette méthode nous permettra d'inscrire un rebelle 
     * à la formation de pilote
     */
        public static Pilote getPiloteNum(int identifiant) throws Exception
    {
        for (Map.Entry<Integer, Pilote> entry: pilote.entrySet()){
            if(entry.getKey()==identifiant){return entry.getValue();}
            
        }
        throw new Exception("Pilote non trouvé");
    }

    public void afficher()
    {
        for (Map.Entry<Integer, Pilote> entry: pilote.entrySet()){
            System.out.println(entry.getKey() + "/" + entry.getValue());
        }
    }

    public void affecterChasseur(int numC) throws java.lang.Exception
    {
        try{
            if(this.chasseur==null){
            this.chasseur=Chasseur.getChasseurNum(numC);}
            else{throw new Exception("Ecraser Chasseur");}
        }catch(Exception e){
            throw e;
        }
    }
    
    public Chasseur getChasseur(){
        return chasseur;
    }
    

    public static Map getListe()
    {
           return pilote;
        
    }
    
    /*@Override
     public String toString()
    {
        return "identifiant"+ identifiant+ "prenom"+ prenom +"nom"+ nom +"age"+ age;
    }*/
}
